package siglas2.redecard.siglas;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<Sigla> siglas = new ArrayList<>();

        try {
            InputStream inputStream = getAssets().open("siglas.json");
            Gson gson = new Gson();

            Type listType = new TypeToken<ArrayList<Sigla>>() {
            }.getType();
            ArrayList arrayList = gson.fromJson(new InputStreamReader(inputStream), listType);
            siglas.addAll(arrayList);
        } catch (IOException e) {
            e.printStackTrace();
        }

        final ListView l1 = findViewById(R.id.list1);
        l1.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return siglas.size();
            }

            @Override
            public Object getItem(int i) {
                return siglas.get(i);
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                LayoutInflater layoutInflater = getLayoutInflater();
                View v = layoutInflater.inflate(R.layout.listitem, null);
                TextView txSiglaView = v.findViewById(R.id.txSigla);
                TextView txSiglaDsc = v.findViewById(R.id.txDescr);
                txSiglaView.setText(siglas.get(i).getSiglaRede());
                txSiglaDsc.setText(siglas.get(i).getNome());

                return v;
            }
        });

        l1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getBaseContext(), DetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("sigla", siglas.get(i));
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        final SwipeRefreshLayout swl = findViewById(R.id.swiperefresh);
        swl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onRefresh() {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {


                        try {
                            String siglasJson = run("https://siglas-8473d.firebaseio.com/siglas.json?print=pretty");
                            Gson gson = new Gson();

                            Type listType = new TypeToken<ArrayList<Sigla>>() {
                            }.getType();
                            ArrayList arrayList = gson.fromJson(siglasJson, listType);
                            siglas.addAll(arrayList);
                            l1.getAdapter().;
                        } catch (IOException e) {
                            Log.e("siglasJson", e.getMessage());
                        }

                        return null;
                    }
                };
            }
        });

    }

    String run(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

}
