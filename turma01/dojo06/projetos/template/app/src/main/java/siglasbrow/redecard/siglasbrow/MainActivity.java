package siglasbrow.redecard.siglasbrow;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        new LoadSiglasTask(this).execute();


        SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        Log.i("SiglasBrow", "onRefresh called from SwipeRefreshLayout");
                        // This method performs the actual data-refresh operation.
                        // The method calls setRefreshing(false) when it's finished.
                        new LoadSiglasTask(MainActivity.this).execute();
                    }
                }
        );

    }

    public static class LoadSiglasTask extends AsyncTask<Void, Void, List<Sigla>> {
        OkHttpClient client = new OkHttpClient();

        WeakReference<MainActivity> ref;

        public LoadSiglasTask(MainActivity mainActivity) {
            ref = new WeakReference<MainActivity>(mainActivity);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            MainActivity mainActivity = ref.get();
            if (mainActivity != null) {
                mainActivity.loadingSiglas();
            }
        }

        @Override
        protected List<Sigla> doInBackground(Void... voids) {
            try {

                Log.d("SiglasBrow", "Carregando siglas");

                Request request = new Request.Builder().url("http://10.0.2.2:3000/siglas").build();
//                    Request request = new Request.Builder().url("http://localhost:3000/siglas").build();
//                    Request request = new Request.Builder().url("https://siglas-8473d.firebaseio.com/siglas.json?print=pretty").build();
                Response response = client.newCall(request).execute();

                String json = response.body().string();

                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<Sigla>>() {
                }.getType();
                ArrayList<Sigla> siglas = gson.fromJson(json, listType);

                Log.d("SiglasBrow", "load " + siglas.size() + " siglas");

                return siglas;
            } catch (Exception e) {
                Log.e("SiglasBrow", "Siglas loading error", e);
                return Collections.emptyList();
            }
        }

        @Override
        protected void onPostExecute(List<Sigla> siglas) {
            super.onPostExecute(siglas);
            MainActivity mainActivity = ref.get();
            if (mainActivity != null) {
                mainActivity.showSiglas(siglas);
            }
        }
    }

    private void loadingSiglas() {
        final ListView lv = findViewById(R.id.listView);
        final TextView tv = findViewById(R.id.empty_list);
        SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swiperefresh);

        swipeRefreshLayout.setRefreshing(true);
        lv.setVisibility(View.GONE);
        tv.setVisibility(View.GONE);
    }

    private void showSiglas(final List<Sigla> siglas) {
        Log.d("SiglasBrow", "show Siglas");

        final ListView lv = findViewById(R.id.listView);
        final TextView tv = findViewById(R.id.empty_list);

        if (siglas.isEmpty()) {
            lv.setVisibility(View.GONE);
            tv.setVisibility(View.VISIBLE);
        } else {
            lv.setVisibility(View.VISIBLE);
            tv.setVisibility(View.GONE);

            lv.setAdapter(new BaseAdapter() {

                LayoutInflater layoutInflater = LayoutInflater.from(getBaseContext());

                @Override
                public int getCount() {
                    return siglas.size();
                }

                @Override

                public Sigla getItem(int i) {
                    return siglas.get(i);
                }

                @Override
                public long getItemId(int i) {
                    return i;
                }

                @Override
                public View getView(int i, View view, ViewGroup viewGroup) {
                    View inflateView = layoutInflater.inflate(R.layout.list_item_initials, null);

                    Sigla sigla = siglas.get(i);

                    TextView tvName = inflateView.findViewById(R.id.name);
                    tvName.setText(sigla.getSiglaRede());

                    TextView tvDescription = inflateView.findViewById(R.id.description);
                    tvDescription.setText(sigla.getNome());

                    return inflateView;
                }
            });
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Sigla sigla = siglas.get(i);
                    Log.d("UserInteraction", "Click: " + sigla);
                    openDetailActivity(sigla);
                }
            });
        }

        SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setRefreshing(false);

    }

    private void openDetailActivity(Sigla sigla) {
        Log.d("SiglasBrow", "Open Detail Activity:" + sigla.getNome());
        Intent intent = new Intent(this, DetailActivity.class);
        Bundle extras = new Bundle();
        extras.putSerializable(DetailActivity.Key.Sigla, sigla);
        intent.putExtras(extras);
        startActivity(intent);
    }
}
