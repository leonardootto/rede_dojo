package dojo2.redecard.siglasbrow;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView txtView = findViewById(R.id.qualquer);

        String[] stringArray = getResources().getStringArray(R.array.siglas);
        StringBuilder sb = new StringBuilder();
        for (String str : stringArray) {
            sb.append(str).append(" \n\n");
        }

        txtView.setText(sb.toString());

    }
}
