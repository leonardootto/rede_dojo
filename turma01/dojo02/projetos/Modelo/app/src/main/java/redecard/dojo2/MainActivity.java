package redecard.dojo2;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView = findViewById(R.id.text);

        Resources res = getResources();
        String[] siglas = res.getStringArray(R.array.siglas);

        StringBuffer sb = new StringBuffer();
        for (String sigla : siglas) {
            sb.append(sigla).append(" \n \n");
        }

        textView.setText(sb);
    }
}
