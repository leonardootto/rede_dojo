package dojo2.redecard.carrasapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainCrrsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_crrs);

        TextView textview = findViewById(R.id.siglas);

        String[] array = getResources().getStringArray(R.array.siglas);

        StringBuilder sb = new StringBuilder();

        for (String s : array) {
            sb.append(s).append("\n");
        }

        textview.setText(sb.toString());



    }
}
