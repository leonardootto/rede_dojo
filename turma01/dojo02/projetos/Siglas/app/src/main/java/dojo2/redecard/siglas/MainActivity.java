package dojo2.redecard.siglas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView siglasView = findViewById(R.id.siglas);

        String[] siglas = getResources().getStringArray(R.array.siglas);

        StringBuilder sb = new StringBuilder();
        for (String sigla : siglas) {
            sb.append(sigla).append("\n");
        }
        siglasView.setText(sb.toString());
    }
}
