import java.io.Serializable;

/**
 * Created by dojo on 26/09/17.
 */

public class Sigla implements Serializable{
    private String id;
    private String siglaRede;
    private String siglaProgramoteca;
    private String nome;
    private String dominioFuncional;
    private String distribuidoWebUnix;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSiglaRede() {
        return siglaRede;
    }

    public void setSiglaRede(String siglaRede) {
        this.siglaRede = siglaRede;
    }

    public String getSiglaProgramoteca() {
        return siglaProgramoteca;
    }

    public void setSiglaProgramoteca(String siglaProgramoteca) {
        this.siglaProgramoteca = siglaProgramoteca;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDominioFuncional() {
        return dominioFuncional;
    }

    public void setDominioFuncional(String dominioFuncional) {
        this.dominioFuncional = dominioFuncional;
    }

    public String getDistribuidoWebUnix() {
        return distribuidoWebUnix;
    }

    public void setDistribuidoWebUnix(String distribuidoWebUnix) {
        this.distribuidoWebUnix = distribuidoWebUnix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sigla sigla = (Sigla) o;

        if (id != null ? !id.equals(sigla.id) : sigla.id != null) return false;
        if (siglaRede != null ? !siglaRede.equals(sigla.siglaRede) : sigla.siglaRede != null)
            return false;
        if (siglaProgramoteca != null ? !siglaProgramoteca.equals(sigla.siglaProgramoteca) : sigla.siglaProgramoteca != null)
            return false;
        if (nome != null ? !nome.equals(sigla.nome) : sigla.nome != null) return false;
        if (dominioFuncional != null ? !dominioFuncional.equals(sigla.dominioFuncional) : sigla.dominioFuncional != null)
            return false;
        return distribuidoWebUnix != null ? distribuidoWebUnix.equals(sigla.distribuidoWebUnix) : sigla.distribuidoWebUnix == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (siglaRede != null ? siglaRede.hashCode() : 0);
        result = 31 * result + (siglaProgramoteca != null ? siglaProgramoteca.hashCode() : 0);
        result = 31 * result + (nome != null ? nome.hashCode() : 0);
        result = 31 * result + (dominioFuncional != null ? dominioFuncional.hashCode() : 0);
        result = 31 * result + (distribuidoWebUnix != null ? distribuidoWebUnix.hashCode() : 0);
        return result;
    }
}
