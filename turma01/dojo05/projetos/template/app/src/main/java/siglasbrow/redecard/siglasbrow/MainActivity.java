package siglasbrow.redecard.siglasbrow;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    ArrayList<Sigla> siglas = new ArrayList<>();
    private ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        this.lv = findViewById(R.id.listView);

        try {
            siglas.addAll(new LoadJsonTask(lv).execute().get());
        } catch (Exception e) {
            e.printStackTrace();
        }

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Sigla sigla = siglas.get(i);
                Log.d("UserInteraction", "Click: " + sigla);
                openDetailActivity(sigla);
            }
        });
    }


    public static class LoadJsonTask extends AsyncTask<Void, Void, List<Sigla>> {

        private final OkHttpClient client = new OkHttpClient();
        private ListView listView;

        public LoadJsonTask(ListView listView) {

            this.listView = listView;
        }

        @Override
        protected List<Sigla> doInBackground(Void... voids) {
            Request request = new Request.Builder()
                    .url("https://siglas-8473d.firebaseio.com/siglas.json?print=pretty")
                    .build();

            try {
                Response response = client.newCall(request).execute();

                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);

                String json = response.body().string();

                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<Sigla>>() {
                }.getType();
                ArrayList arrayList = gson.fromJson(json, listType);
                return arrayList;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return Collections.emptyList();
        }
    }

    private void openDetailActivity(Sigla sigla) {
        Intent intent = new Intent(this, DetailActivity.class);
        Bundle extras = new Bundle();
        extras.putSerializable(DetailActivity.Key.Sigla, sigla);
        intent.putExtras(extras);
        startActivity(intent);
    }
}
