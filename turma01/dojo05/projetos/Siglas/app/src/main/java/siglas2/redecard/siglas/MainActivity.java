package siglas2.redecard.siglas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<Sigla> siglas = new ArrayList<>();

        try {
            InputStream inputStream = getAssets().open("siglas.json");
            Gson gson = new Gson();

            Type listType = new TypeToken<ArrayList<Sigla>>() {
            }.getType();
            ArrayList arrayList = gson.fromJson(new InputStreamReader(inputStream), listType);
            siglas.addAll(arrayList);
        } catch (IOException e) {
            e.printStackTrace();
        }

        final ListView l1 = findViewById(R.id.list1);
        l1.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return siglas.size();
            }

            @Override
            public Object getItem(int i) {
                return siglas.get(i);
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                LayoutInflater layoutInflater = getLayoutInflater();
                View v = layoutInflater.inflate(R.layout.listitem, null);
                TextView txSiglaView = v.findViewById(R.id.txSigla);
                TextView txSiglaDsc = v.findViewById(R.id.txDescr);
                txSiglaView.setText(siglas.get(i).getSiglaRede());
                txSiglaDsc.setText(siglas.get(i).getNome());


                return v;
            }
        });

        l1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getBaseContext(), DetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("sigla", siglas.get(i));
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
