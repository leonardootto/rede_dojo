package siglas2.redecard.siglas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.io.Serializable;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(getIntent().hasExtra("sigla")){
            Sigla sigla = (Sigla) getIntent().getSerializableExtra("sigla");
            Log.d("log",sigla.getNome());


            TextView tvIdDetail = findViewById(R.id.tvIdValue);
            tvIdDetail.setText(sigla.getId());

            TextView tvSiglaRedeDetail = findViewById(R.id.tvSiglaRedeValue);
            tvSiglaRedeDetail.setText(sigla.getSiglaRede());

            TextView tvSiglaProg = findViewById(R.id.tvSiglaProg);
            tvSiglaProg.setText(sigla.getSiglaProgramoteca());

            TextView tvNome = findViewById(R.id.tvNome);
            tvNome.setText(sigla.getNome());

            TextView tvDomFunc = findViewById(R.id.tvDomFunc);
            tvDomFunc.setText(sigla.getDominioFuncional());

            TextView tvAmbiente = findViewById(R.id.tvAmbiente);
            tvAmbiente.setText(sigla.getDistribuidoWebUnix());
        }


    }
}
