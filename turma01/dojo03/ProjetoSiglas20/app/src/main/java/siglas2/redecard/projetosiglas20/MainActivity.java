package siglas2.redecard.projetosiglas20;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<Sigla> siglas = new ArrayList<>();

        try {
            InputStream inputStream = getAssets().open("siglas.json");
            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<Sigla>>() {
            }.getType();
            ArrayList arrayList = gson.fromJson(new InputStreamReader(inputStream), listType);
            siglas.addAll(arrayList);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ListView listView = findViewById(R.id.listViewSiglas);
        listView.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return siglas.size();
            }

            @Override
            public Sigla getItem(int i) {
                return siglas.get(i);
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {

                View viewLayout = getLayoutInflater().inflate(R.layout.item_siglas, null);
                TextView tvSigla = viewLayout.findViewById(R.id.tvSigla);
                TextView tvDesc = viewLayout.findViewById(R.id.tvDesc);

                tvSigla.setText(getItem(i).getSiglaRede());
                tvDesc.setText(getItem(i).getNome());
                return viewLayout;
            }
        });
    }
}
