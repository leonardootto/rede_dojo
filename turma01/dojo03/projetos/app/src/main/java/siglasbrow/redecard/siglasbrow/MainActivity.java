package siglasbrow.redecard.siglasbrow;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        final ArrayList<Sigla> siglas = new ArrayList<>();

        try {
            InputStream inputStream = getAssets().open("siglas.json");
            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<Sigla>>() {}.getType();
            ArrayList arrayList = gson.fromJson(new InputStreamReader(inputStream), listType);
            siglas.addAll(arrayList);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ListView lv = findViewById(R.id.listView);
        lv.setAdapter(new BaseAdapter() {

            LayoutInflater layoutInflater = LayoutInflater.from(getBaseContext());

            @Override
            public int getCount() {
                return siglas.size();
            }

            @Override

            public Sigla getItem(int i) {
                return siglas.get(i);
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                View inflateView = layoutInflater.inflate(R.layout.list_item_initials, null);

                Sigla sigla = siglas.get(i);

                TextView tvName = inflateView.findViewById(R.id.name);
                tvName.setText(sigla.getSiglaRede());

                TextView tvDescription = inflateView.findViewById(R.id.description);
                tvDescription.setText(sigla.getNome());

                return inflateView;
            }
        });
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Sigla sigla = siglas.get(i);
                Log.d("UserInteraction", "Click: " + sigla);
                openDetailActivity(sigla);
            }
        });
    }

    private void openDetailActivity(Sigla sigla) {
        Intent intent = new Intent(this, DetailActivity.class);
        Bundle extras = new Bundle();
        extras.putSerializable(DetailActivity.Key.Sigla, sigla);
        intent.putExtras(extras);
        startActivity(intent);
    }
}
