package siglasbrow.redecard.siglasbrow;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dojo on 26/09/17.
 */

public class Regexer {
    public static void main(String[] args) throws Exception {
        BufferedReader fileReader = new BufferedReader(new FileReader("/Users/dojo/Desktop/dojos/dojo3/projetos/siglas.csv"));
        Pattern pattern = Pattern.compile("(\\d{1,3});(\\w{1,5});(\\w{1,5});(.+)?;(.+)?;(.+)?;(.*)");

        String line;
        while ((line = fileReader.readLine()) != null) {
            Matcher matcher = pattern.matcher(line);

            if (matcher.find()) {
                String nro = matcher.group(1);
                String siglaRede = matcher.group(2);
                String siglaProgramoteca = matcher.group(3);
                String nome = matcher.group(4);
                String dominioFuncional = matcher.group(5);
                String mainframe = matcher.group(6);
                String distribuidoWebUnix = matcher.group(7);

                String json = "{\n" +
                        "  \"id\":\"" + nro + "\",\n" +
                        "  \"siglaRede\":\"" + siglaRede + "\",\n" +
                        "  \"siglaProgramoteca\":\"" + siglaProgramoteca + "\",\n" +
                        "  \"nome\":\"" + nome + "\",\n" +
                        "  \"dominioFuncional\":\"" + dominioFuncional + "\",\n" +
                        "  \"mainframe\":\"" + mainframe + "\",\n" +
                        "  \"distribuidoWebUnix\":\"" + distribuidoWebUnix + "\"\n" +
                        "},";

                System.out.println(json);
            } else {
                System.out.println("Dont find");
            }

        }
    }
}
