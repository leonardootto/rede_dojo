package siglasbrow.redecard.siglasbrow;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.io.Serializable;

public class DetailActivity extends AppCompatActivity {

    public static final class Key {
        public static final String Sigla = "Sigla";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        TextView txtId = findViewById(R.id.txtId);
        TextView txtSiglaRede = findViewById(R.id.txtSiglaRede);
        TextView txtSiglaProgramoteca = findViewById(R.id.txtSiglaProgramoteca);
        TextView txtNome = findViewById(R.id.txtNome);
        TextView txtDominioFuncinal = findViewById(R.id.txtDominioFuncinal);
        TextView txtDistribuidoWebUnix = findViewById(R.id.txtDistribuidoWebUnix);

        if (getIntent().hasExtra(Key.Sigla)) {
            Sigla sigla = (Sigla) getIntent().getSerializableExtra(Key.Sigla);
            getSupportActionBar().setTitle(sigla.getSiglaRede());

            txtId.setText(sigla.getId());
            txtSiglaRede.setText(sigla.getSiglaRede());
            txtSiglaProgramoteca.setText(sigla.getSiglaProgramoteca());
            txtNome.setText(sigla.getNome());
            txtDominioFuncinal.setText(sigla.getDominioFuncional());
            txtDistribuidoWebUnix.setText(sigla.getDistribuidoWebUnix());
        }else{
            txtId.setText("-");
            txtSiglaRede.setText("-");
            txtSiglaProgramoteca.setText("-");
            txtNome.setText("-");
            txtDominioFuncinal.setText("-");
            txtDistribuidoWebUnix.setText("-");
        }
    }

}
